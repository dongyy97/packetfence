import { BaseViewCollectionItem } from '../../_components/new/'
import {
  BaseFormButtonBar,
  BaseFormGroupChosenMultiple,
  BaseFormGroupInput
} from '@/components/new/'
import {
  BaseFormGroupIntervalUnit,
  BaseFormGroupOses
} from '@/views/Configuration/_components/new/'
import BaseFormGroupNetworks from './BaseFormGroupNetworks'
import TheForm from './TheForm'
import TheView from './TheView'

export {
  BaseViewCollectionItem      as BaseView,
  BaseFormButtonBar           as FormButtonBar,

  BaseFormGroupInput          as FormGroupIdentifier,
  BaseFormGroupInput          as FormGroupDescription,
  BaseFormGroupInput          as FormGroupSecret,
  BaseFormGroupNetworks       as FormGroupNetworks,

  TheForm,
  TheView
}
